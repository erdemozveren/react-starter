/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  Alert,
  ActivityIndicator
} from 'react-native';
import { Overlay,Screen,View,NavigationBar,Image,ImageBackground,Card,Divider,Caption,Tile,Title,Icon,Subtitle,Heading,Button, TouchableOpacity } from '@shoutem/ui';
import {inject,observer} from 'mobx-react';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};


function renderCard(data,i) {
  return (<Tile style={{margin:3}} >
    
  <TouchableOpacity onPress={()=>{
    this.props.navigation.navigate('Post',{
      title:data.item.title,
      body:data.item.body,
      image:"https://picsum.photos/400/?image"+data.item.id
    });
  }}>
    <Image
      styleName="large-banner"
      source={{uri:"https://picsum.photos/400/?image"+data.item.id}}
    />
    </TouchableOpacity>
    <View styleName="content">
      <Subtitle>{data.item.title}</Subtitle>
      <Caption>{data.item.body}</Caption>
      <View styleName="horizontal space-between">
        <Caption>{data.item.id} saat önce</Caption>
        <Caption>{Math.ceil(Math.random()*1)+''+Math.ceil(Math.random()*9)}:34</Caption>
      </View>
    </View>
  </Tile>);
}
function keyMaker(data){
  return data.id+'';
}
@inject(['store'])
@observer
export default class Home extends Component<Props> {
  static navigationOptions = {
    title: 'Kemal Rehberi',
  };
  constructor(props){
    super(props);
    this.state={posts:[],loaded:false};
  }
  componentWillMount(){
    this.props.store.posts.fetchData();
  }
  render() {
    return (
  <Screen >
  <FlatList numColumns={2} /*contentContainerStyle={{padding:10}}*/ data={this.props.store.posts.posts} keyExtractor={keyMaker} renderItem={renderCard.bind(this)}>
  </FlatList>
  </Screen>
    );
  }
}
