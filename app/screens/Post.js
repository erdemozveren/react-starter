/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  Alert,
  ActivityIndicator
} from 'react-native';
import { Overlay,Screen,View,NavigationBar,Image,ImageBackground,Card,Divider,Caption,Tile,Title,Icon,Subtitle,Heading,Button, TouchableOpacity } from '@shoutem/ui';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

export default class Post extends Component<Props> {
  static navigationOptions = {
    title: 'Post Kardeşim',
  };
  render() {
    const { navigation } = this.props;
    return (
      <Tile style={{margin:3}} >
    
  <TouchableOpacity onPress={()=>{
    Alert.alert('Slm');
  }}>
    <Image
      styleName="large-banner"
      source={{uri:navigation.getParam('image')}}
    />
    </TouchableOpacity>
    <View styleName="content">
      <Subtitle>{navigation.getParam('title')}</Subtitle>
      <Caption>{navigation.getParam('body')}</Caption>
      <View styleName="horizontal space-between">
        <Caption>1 saat önce</Caption>
        <Caption>{Math.ceil(Math.random()*2)+''+Math.ceil(Math.random()*9)}:34</Caption>
      </View>
    </View>
  </Tile>
    );
  }
}
