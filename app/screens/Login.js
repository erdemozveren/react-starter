/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  Alert,
  ActivityIndicator,
  TextInput
} from 'react-native';
import { Overlay,Screen,View,NavigationBar,Image,ImageBackground,Card,Divider,Caption,Tile,Title,Icon,Subtitle,Heading,Button, TouchableOpacity } from '@shoutem/ui';

var t = require('tcomb-form-native');
var Form = t.form.Form;
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

var User = t.struct({
  name: t.String,              // a required string
  surname: t.maybe(t.String),  // an optional string
  age: t.Number,               // a required number
  rememberMe: t.Boolean        // a boolean
});



type Props = {};

export default class Post extends Component<Props> {
  static navigationOptions = {
    title: 'Post Kardeşim',
    header:null,
  };
  render() {
    const { navigation } = this.props;
    return ( <Form
      ref="form"
      type={User}
    />);
  }
}
