/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  Alert,
  ActivityIndicator,
  YellowBox
} from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import {createStackNavigator} from 'react-navigation';
import { Overlay,Screen,View,NavigationBar,Image,ImageBackground,Card,Divider,Caption,Tile,Title,Icon,Subtitle,Heading,Button, TouchableOpacity } from '@shoutem/ui';
import Home from './app/screens/Home';
import Post from './app/screens/Post';
import Login from './app/screens/Login';
import Store from './app/stores';
import {Provider} from 'mobx-react';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};


const MainNavigator = createStackNavigator({
  Home: {screen: Home},
  Post: {screen: Post},
  Login: {screen: Login},
}, {mode: 'modal',initialRouteName:'Login',
navigationOptions:{
  header:(props)=>(<ImageBackground
    source={require('./img/image-3.png')}
    style={{  height: 70 }}
  >
    <NavigationBar
      styleName="clear"
      centerComponent={<Title>{props.scene.descriptor.options.title}</Title>}
      leftComponent={<Button onPress={()=>{props.navigation.goBack(null)}}>
        <Icon name="back" />
      </Button>}
    />
  </ImageBackground>)
}});
export default class App extends Component<Props> {
  render() {
    return (
      /*<View>
         <ImageBackground
  source={require('./img/image-3.png')}
  style={{  height: 70 }}
>
  <NavigationBar
    styleName="clear"
    centerComponent={<Title>BAŞLIK KARDEEEŞ</Title>}
  />
</ImageBackground>*/
<Provider store={Store}>
<MainNavigator />
</Provider>
      //</View>
    );
  }
}
