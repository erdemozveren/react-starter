import {observable,action} from 'mobx'


class PostsStore {
  @observable posts = []

  @action fetchData() {
    fetch('https://jsonplaceholder.typicode.com/posts').then(e=>e.json()).then(e=>{
      console.log('Yükledk be babam');
      this.posts=e;
    });
  }
}

export default new PostsStore()